#!/bin/bash

msg(){
case $1 in
 -bar)echo -e "\e[1;30m================================================";;
esac
}

sysctl -w net.ipv6.conf.all.disable_ipv6=1 && sysctl -p
echo 'net.ipv6.conf.all.disable_ipv6 = 1' > /etc/sysctl.d/70-disable-ipv6.conf
sysctl -p -f /etc/sysctl.d/70-disable-ipv6.conf

if [[ ! -e ~/.conf-local ]]; then
add-apt-repository universe
apt update -y
apt upgrade -y

install_ini() {
    clear
    msg -bar
    echo -e "\033[92m        -- INSTALANDO PAQUETES NECESARIOS -- "
    msg -bar
    ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    locale-gen en_US.UTF-8 >/dev/null 2>&1
    update-locale LANG=en_US.UTF-8 >/dev/null 2>&1
    echo -e "\033[97m  # Instalando  UTF...................... $ESTATUS "
    apt-get install gawk -y >/dev/null 2>&1
    #bc
    [[ $(dpkg --get-selections | grep -w "jq" | head -1) ]] || apt-get install jq -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "jq" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "jq" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install jq................... $ESTATUS "
    #SCREEN
    [[ $(dpkg --get-selections | grep -w "screen" | head -1) ]] || apt-get install screen -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "screen" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "screen" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install screen............... $ESTATUS "
    #apache2
    [[ $(dpkg --get-selections | grep -w "apache2" | head -1) ]] || {
        apt-get install apache2 -y &>/dev/null
        sed -i "s;Listen 80;Listen 81;g" /etc/apache2/ports.conf
        service apache2 restart >/dev/null 2>&1 &
    }
    [[ $(dpkg --get-selections | grep -w "apache2" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "apache2" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install apache2.............. $ESTATUS "
    #curl
    [[ $(dpkg --get-selections | grep -w "curl" | head -1) ]] || apt-get install curl -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "curl" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "curl" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install curl................. $ESTATUS "
    #socat
    [[ $(dpkg --get-selections | grep -w "socat" | head -1) ]] || apt-get install socat -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "socat" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "socat" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install socat................ $ESTATUS "
    #netcat
    [[ $(dpkg --get-selections | grep -w "netcat" | head -1) ]] || apt-get install netcat -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "netcat" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "netcat" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install netcat............... $ESTATUS "
    #netcat-traditional
    [[ $(dpkg --get-selections | grep -w "netcat-traditional" | head -1) ]] || apt-get install netcat-traditional -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "netcat-traditional" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "netcat-traditional" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install netcat-traditional... $ESTATUS "
    #net-tools
    [[ $(dpkg --get-selections | grep -w "net-tools" | head -1) ]] || apt-get install net-tools -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "net-tools" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "net-tools" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install net-tools............ $ESTATUS "
    #cowsay
    [[ $(dpkg --get-selections | grep -w "cowsay" | head -1) ]] || apt-get install cowsay -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "cowsay" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "cowsay" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install cowsay............... $ESTATUS "
    #figlet
    [[ $(dpkg --get-selections | grep -w "figlet" | head -1) ]] || apt-get install figlet -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "figlet" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "figlet" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install figlet............... $ESTATUS "
    #lolcat
    apt-get install lolcat -y &>/dev/null
    sudo gem install lolcat &>/dev/null
    [[ $(dpkg --get-selections | grep -w "lolcat" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "lolcat" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install lolcat............... $ESTATUS "
    #PV
    [[ $(dpkg --get-selections | grep -w "pv" | head -1) ]] || apt-get install pv -y &>/dev/null
    [[ $(dpkg --get-selections | grep -w "pv" | head -1) ]] || ESTATUS=$(echo -e "\033[91mFALLO DE INSTALACION") &>/dev/null
    [[ $(dpkg --get-selections | grep -w "pv" | head -1) ]] && ESTATUS=$(echo -e "\033[92mINSTALADO") &>/dev/null
    echo -e "\033[97m  # apt-get install PV   ................ $ESTATUS "
    msg -bar
    echo -e "\033[92m La instalacion de paquetes necesarios a finalizado"
    msg -bar
    echo -e "\033[97m Si la instalacion de paquetes tiene fallas"
    echo -ne "\033[97m     Reintentar Install Paquetes [ s/n ]: "
    read inst
    [[ $inst = @(s|S|y|Y) ]] && install_ini
}
install_ini
touch ~/.conf-local
else
	msg -bar
	msg -ne 'reinstalar paquetes?: '&&read ok
	[[ $ok == @('s'|'S'|'Si'|'si') ]] && rm ~/.conf-local&&source `pwd`/$0 || echo 'ok'
fi
apt reinstall ufw -y
sudo ufw allow 81
sudo ufw allow 8888

[[ -e /bin/http-server.sh ]] && rm /bin/http-server.sh
wget -O /usr/local/sbin/http-server.sh https://gitlab.com/donpatobot/dlbt/-/raw/main/sbin/http-server.sh &> /dev/null
chmod +x /usr/local/sbin/http-server.sh
[[ ! -d /etc/adm-db ]] && mkdir /etc/adm-db
wget -O /etc/adm-db/BotGen.sh https://gitlab.com/donpatobot/dlbt/-/raw/main/BotGen.sh &> /dev/null
read -p $'id: ' id
read -p $'token: ' token
echo $id > /etc/adm-db/Admin-ID
echo $token > /etc/adm-db/token

for srv in `echo "/etc/systemd/system/botgen.service /etc/systemd/system/http-server.service"`; do
	serv=$(echo ${srv##*/}|awk -F "." '{print $1}')
	[[ -e ${srv} ]] && {
		systemctl stop ${serv}
		systemctl disable ${serv}
		systemctl daemon-reload
		rm -f $srv
	}
	wget -O ${srv} https://gitlab.com/donpatobot/dlbt/-/raw/main/otros/service/${srv##*} &> /dev/null
	systemctl enable ${serv}
	systemctl start ${serv}
	systemctl daemon-reload
done
